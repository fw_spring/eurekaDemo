# eurekaDemo

### 项目介绍
Eureka 微服务开发， 用于测试。

### 服务的注册与发现
> 关系调用说明：
> - 服务提供者启动时，向服务注册中心注册自己提供的服务
> - 服务消费者启动时，在服务注册中心订阅自己所需要的服务
> - 注册中心返回服务提供者的地址信息给消费者
> - 消费者从提供者中调用服务

### Eureka简介
> `Eureka`是Spring Cloud Netflix微服务套件中的一部分，可以与Springboot构建的微服务很容易的整合起来。  
> Eureka包含了服务器端和客户端组件。服务器端，也被称作是服务注册中心，用于提供服务的注册与发现。Eureka支持高可用的配置，当集群中有分片出现故障时，Eureka就会转入`自动保护模式`，它允许分片故障期间继续提供服务的发现和注册，当故障分片恢复正常时，集群中其他分片会把他们的状态再次同步回来。  
> 客户端组件包含服务消费者与服务提供者。在应用程序运行时，Eureka客户端向注册中心注册自身提供的服务并周期性的发送心跳来更新它的服务租约。同时也可以从服务端查询当前注册的服务信息并把他们缓存到本地并周期性的刷新服务状态。  

### Eureka服务注册中心
> - 添加`@EnableEurekaServer`注解
> - `application.yml`配置  

    server:
      port: 3100
    eureka:
      instance:
        hostname: localhost
      client:
        #不要向注册中心注册自己    
        register-with-eureka: false
        #禁止检索服务
        fetch-registry: false
        service-url:
          defaultZone: http://${eureka.instance.hostname}:${server.port}/eureka

>  - 通多`${eureka.instance.hostname}:${server.port}` 访问Eureka服务注册中心

### Eureka Server搭建
- Spring Cloud 版本： Finchley.SR2, 引入`spring-cloud-starter-netflix-eureka-server` , pom.xml配置如下：

      <parent>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-parent</artifactId>
        <version>Finchley.SR2</version>
        <relativePath/>
      </parent>

      <dependencies>
        <!--添加Eureka服务器端依赖-->
        <dependency>
          <groupId>org.springframework.cloud</groupId>
          <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
        </dependency>
      </dependencies>

- 添加`@EnableEurekaClient` 注解, application实例：

      @EnableEurekaServer
      @SpringBootApplication
      public class EurekaServerApplication {
          public static void main(String[] args) {
              SpringApplication.run(EurekaServerApplication.class, args);
          }
      }

> - `application.yml` 配置

    #注册中心
    eureka:
      client:
        serviceUrl:
          defaultZone: http://localhost:3100/eureka/
      #使用IP访问注册中心
      instance:
        prefer-ip-address: true
            
>  - 通过`${eureka.instance.hostname}:${server.port}` 查看服务注册情况

### Eureka注册服务客户端搭建（Ribbon实现负载均衡）
> - 引入`spring-cloud-starter-netflix-eureka-server` 包
> - 添加`@EnableEurekaClient` 注解  
> - `application.yml` 配置（与服务提供者相同）
> - 开启负载均衡

    @Bean   //将此Bean交给spring容器
    @LoadBalanced   //通过此注解开启负载均衡
    RestTemplate restTemplate() {
        return new RestTemplate();
    }
    
> - 微服务调用实现

    @RestController
    @RequestMapping("/ribbon")
    public class RibbonController {
    
        @Autowired
        //注入restTemplate
        private RestTemplate restTemplate;
    
        @RequestMapping(value="/hello", method= RequestMethod.GET)
        public String hello() {
            //使用restTemplate调用微服务接口
            return restTemplate.getForEntity("http://spring-cloud-config-server/hello", String.class).getBody();
        }
    }
