package com.fwpsl.spring;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@EnableEurekaServer
@SpringBootApplication
@RestController
public class EurekaServerApplication {

    @Value("${spring.application.name:}")
    private String appName;

    @Value("${server.port}")
    private String appPort;

    @RequestMapping("/welcome")
    public String hello() {
        return String.format("Welcome to Eureka Service! AppName: %s, AppPort: %s", appName, appPort);
    }

    public static void main(String[] args) {
        SpringApplication.run(EurekaServerApplication.class, args);
    }
}
